
defmodule Main do



  
  def first_part(), do:
    (with {:ok, coordinates_csv} <- File.read("coordinates.csv"),
      {:ok, coords2} <- coordinates_csv |> parser()
      # "Assign every coordinate to one of the previously computed bounding boxes given
      # that the coordinate is inside a bounding box."
	do coords2
	|> Enum.map(fn c ->
	  {c, boxes_from_file() |> find_box(c) }
	end)
      # "If no bounding box matches, just discard the coordinate."
      |> Enum.filter(fn {_c, box} ->
	  box != nil
	end)
	|> Map.new()
	# assign every coordinate to *1* box
	# map coordinates to a box
	else
	  {:error, msg} -> {:file_error, msg}
        {:parse_errors, es} -> {:parse_errors, es}
      end)  




    
  # Other options:
  # 1) use monadic parsers instead...
  # 2) use a csv library
  defp parser(s) when is_binary(s), do:
    s
    |> String.trim()
    # _assume_ unix
    |> String.split("\n")
    # remove header (any column funny-business in file will cause error at runtime, for user to solve)
    |> fn ["lon,lat" | t] ->
      # parse rows
      t
      |> Enum.map(fn s ->
	String.split(s, ",")
        # parse numbers
	|> Enum.map( fn s ->
	  Float.parse(s)
	  |> (case do
		{f, _s} -> f |> Float.round(5) 
		# return original string
		_e -> s
	      end)
	end)
	|> List.to_tuple()
      end)
    end.()
    # group errors / successes
    |> Enum.reduce(
      %{ok: [], parse_errors: []}, 
      fn c, a ->
	case c do
	  # Assume that coord numbers are OK... Should check they are within specified ranges here
	  {f1, f2} when is_float(f1) and is_float(f2) -> %{ok: a.ok ++ [c], parse_errors: a.parse_errors}
	  _e -> %{ok: a.ok, parse_errors: a.parse_errors ++ [c]}
	end
      end)
    # return errors only, if they exist
    |> fn %{parse_errors: pes, ok: oks} ->
      case pes do
	[] -> {:ok, oks}
	_ -> {:parse_errors, pes}
      end
    end.()



    
  # should prob be [[{},{}]] instead of [{{},{}}], but this is easier
  # should probably 'normalise' boxes, could make things much more efficient / simpler
  defp boxes(cs), do:
    cs
    |> fn [h|t] -> Enum.zip(t, [h|t]) end.()


  def boxes_from_file(), do:
    (with {:ok, pairs_csv} <- File.read("pairs.csv"),
      {:ok, coords} <- pairs_csv |> parser()
       do
         coords |> boxes()
       else
         {:parse_errors, es} -> {:parse_errors, es}
     end)


    
    
  # note: paired coordinates have to be diagonally opposite, otherwise they'd only specify a line...
  # "coordinate is inside a bounding box." i.e. "<" not "<=" etc.  
  defp within_box?({{lon1,lat1},{lon2,lat2}}, {lonP,latP}) when lon1 < lonP and lonP < lon2 and lat1 < latP and latP < lat2, do:
    true
  defp within_box?({{lon1,lat1},{lon2,lat2}}, {lonP,latP}) when lon1 < lonP and lonP < lon2 and lat2 < latP and latP < lat1, do:
    true
  defp within_box?({{lon1,lat1},{lon2,lat2}}, {lonP,latP}) when lon2 < lonP and lonP < lon1 and lat2 < latP and latP < lat1, do:
    true
  defp within_box?({{lon1,lat1},{lon2,lat2}}, {lonP,latP}) when lon2 < lonP and lonP < lon1 and lat1 < latP and latP < lat2, do:
    true
  defp within_box?({{lon1,lat1},{lon2,lat2}}, {lonP,latP}), do:
    false


    

  def match_up(origin, destination, boxes), do:
    %{origin_coordinate: origin,
      origin_box: find_box(boxes, origin),
      destination_coordinate: destination,
      destination_box: find_box(boxes, destination)
    } 

  defp find_box(boxes, coord), do:
    Enum.find(boxes, fn b -> within_box?(b, coord) end) 


    
end




defmodule Server do
  
  use GenServer
  
  # "create a module that keeps track of every bounding box previously created"
  @impl true
  def init(:ok), do:
    {:ok, %{boxes: Main.boxes_from_file(), matches: []}}

  @impl true
  def handle_call(:status, _from, state), do:
    {:reply, state, state}

  @impl true
  def handle_cast({:match, origin, destination}, state), do:
    {:noreply, %{ matches: state.matches ++ [ Main.match_up(origin, destination, state.boxes) ], boxes: state.boxes } }

  # --
  
  def start_link(), do:
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)

  # "can take a pair of geographic coordinates as argument (i.e. origin and destination)"    
  def match(origin, destination), do:
    GenServer.cast(__MODULE__, {:match, origin, destination})

  # "return the matching bounding boxes (for either origin, destination or both)"
  # "It should also build and store the corresponding bounding box for each received pair."
  # --> stored in server memory
  def status(), do:
    GenServer.call(__MODULE__, :status)
  
end
